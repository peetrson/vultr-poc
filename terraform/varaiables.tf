variable "vultr2_api_key" {
  type      = string
  sensitive = true
}

variable "domain_name" {
  type    = string
  default = "chrchel.eu"
}
