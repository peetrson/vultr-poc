provider "vultr" {
  rate_limit  = 1000
  retry_limit = 3
}

provider "vultr" {
  rate_limit  = 700
  retry_limit = 3
  api_key     = var.vultr2_api_key
  alias       = "vultr_proper"
}

provider "helm" {
  kubernetes {
    host = local.out.k8s.config.host

    cluster_ca_certificate = local.out.k8s.config.cluster_ca_certificate
    client_certificate     = local.out.k8s.config.client_certificate
    client_key             = local.out.k8s.config.client_key
  }
}

provider "kubernetes" {
  host = local.out.k8s.config.host

  cluster_ca_certificate = local.out.k8s.config.cluster_ca_certificate
  client_certificate     = local.out.k8s.config.client_certificate
  client_key             = local.out.k8s.config.client_key
}
