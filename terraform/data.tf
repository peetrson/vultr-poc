data "vultr_account" "my_account" {}

data "vultr_region" "region" {
  filter {
    name   = "city"
    values = [local.vultr.city]
  }
}

data "vultr_plan" "plan" {
  filter {
    name   = "type"
    values = ["vc2"]
  }
  filter {
    name   = "ram"
    values = ["2048"]
  }
  filter {
    name   = "vcpu_count"
    values = ["1"]
  }
  filter {
    name   = "locations"
    values = [data.vultr_region.region.id]
  }
}

/*
data "vultr_instance" "k8s_nodes" {
  for_each = toset([for instance in vultr_kubernetes.k8s.node_pools[0].nodes : instance.id])

  filter {
    name   = "id"
    values = [each.value]
  }

  depends_on = [
    vultr_kubernetes.k8s,
  ]
}
*/

data "vultr_dns_domain" "domain" {
  domain = var.domain_name

  provider = vultr.vultr_proper
}


data "kubernetes_service" "ingress_controller" {
  metadata {
    namespace = helm_release.ingress_controller.namespace
    name      = "${helm_release.ingress_controller.name}-${helm_release.ingress_controller.chart}-controller"
  }
}
