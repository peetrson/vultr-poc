output "account" {
  value = data.vultr_account.my_account
}

output "region" {
  value = data.vultr_region.region
}

output "plan" {
  value = data.vultr_plan.plan
}

output "node_ips" {
  value = local.out.k8s.node_pool.ips
}


/*
output "ingress_ip" {
  value = data.kubernetes_service.ingress_controller.status.0.load_balancer.0.ingress.0.ip
}
*/
