resource "vultr_kubernetes" "k8s" {
  region  = data.vultr_region.region.id
  label   = "poc"
  version = local.k8s.version

  node_pools {
    node_quantity = 1
    plan          = data.vultr_plan.plan.id
    label         = "poc"
  }
}

resource "local_file" "k8s_config" {
  filename = "${local.out.directory}/k8s_config.yml"
  content_base64 = vultr_kubernetes.k8s.kube_config
}

resource "kubernetes_namespace" "ingress_controller" {
  metadata {
    name = "ingress-controller"
  }
}

# https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx
resource "helm_release" "ingress_controller" {
  name       = "nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = "4.4.0"

  namespace = kubernetes_namespace.ingress_controller.metadata.0.name

  set {
    name  = "controller.ingressClassResource.default"
    value = "true"
  }

  set {
    name  = "controller.ingressClass"
    value = local.k8s.ingress.class
  }

  dynamic "set" {
    for_each = local.k8s.cert_manager.enabled ? [module.cert_manager[0].default_ssl_certificate] : []
    content {
      name  = "controller.extraArgs.default-ssl-certificate"
      value = set.value
    }
  }
}

module "cert_manager" {
  count = local.k8s.cert_manager.enabled ? 1 : 0

  source = "./modules/cert_manager"

  domain_name = var.domain_name
  vultr_api_key = var.vultr2_api_key
}
