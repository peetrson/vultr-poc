resource "vultr_dns_record" "ingress" {
  domain = data.vultr_dns_domain.domain.id
  name   = "miobjegumidek"
  data   = data.kubernetes_service.ingress_controller.status.0.load_balancer.0.ingress.0.ip
  type   = "A"

  provider = vultr.vultr_proper
}

resource "vultr_dns_record" "ingress_others" {
  for_each = toset(["jitrnica", "bambula"])

  domain = data.vultr_dns_domain.domain.id
  name   = each.key
  data   = data.kubernetes_service.ingress_controller.status.0.load_balancer.0.ingress.0.ip
  type   = "A"

  provider = vultr.vultr_proper
}


resource "vultr_dns_record" "ingress_wildcard" {
  domain = data.vultr_dns_domain.domain.id
  name   = "*.${vultr_dns_record.ingress.name}"
  data   = vultr_dns_record.ingress.data
  type   = "A"

  provider = vultr.vultr_proper
}
