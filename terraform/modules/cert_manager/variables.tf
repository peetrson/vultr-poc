variable "namespace" {
  default = "cert-manager"
}
variable "domain_name" {
  type = string
}
variable "vultr_api_key" {
  type = string
}
