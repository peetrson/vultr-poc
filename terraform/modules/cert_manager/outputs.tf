output "namespace" {
  value = var.namespace
}

output "default_ssl_certificate" {
  value = "${kubernetes_manifest.certificate.manifest.metadata.namespace}/${kubernetes_manifest.certificate.manifest.spec.secretName}"
}
