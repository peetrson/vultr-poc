resource "kubernetes_namespace" "cert_manager" {
  metadata {
    name = var.namespace
  }
}

resource "helm_release" "cert_manager" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v1.10.0"

  namespace = kubernetes_namespace.cert_manager.metadata.0.name

  set {
    name  = "installCRDs"
    value = "true"
  }
}

resource "kubernetes_secret" "vultr_domain" {
  metadata {
    name      = "vultr-credentials"
    namespace = kubernetes_namespace.cert_manager.metadata.0.name
  }

  data = {
    api_key = var.vultr_api_key
  }
}

resource "kubernetes_manifest" "letsencrypt_prod_issuer" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"

    metadata = {
      name = "letsencrypt-prod-issuer"
    }

    spec = {
      acme = {
        server = "https://acme-v02.api.letsencrypt.org/directory"
        email  = "peetrson@gmail.com"
        privateKeySecretRef = {
          name = "letsencrypt-production"
        }
        solvers = [
          {
            dns01 = {
              webhook = {
                groupName  = "acme.vultr.com"
                solverName = "vultr"
                config = {
                  apiKeySecretRef = {
                    name = kubernetes_secret.vultr_domain.metadata.0.name
                    key  = "api_key" # TODO
                  }
                }
              }
            },
            # DNS in first place
            #          {
            #            http01 = {
            #              ingress = {
            #                class = local.k8s.ingress.class
            #              }
            #            }
            #          },
          },
        ]
      }
    }
  }

  depends_on = [
    helm_release.cert_manager,
  ]
}

resource "kubernetes_manifest" "certificate" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "Certificate"

    metadata = {
      name      = var.domain_name
      namespace = kubernetes_namespace.cert_manager.metadata.0.name
    }

    spec = {
      secretName = "ingress-cert-${var.domain_name}"
      issuerRef  = {
        kind = kubernetes_manifest.letsencrypt_prod_issuer.manifest.kind
        name = kubernetes_manifest.letsencrypt_prod_issuer.manifest.metadata.name
      }
      commonName = var.domain_name
      dnsNames   = [
        var.domain_name,
        "*.${var.domain_name}",
      ]
    }
  }

  depends_on = [
    helm_release.cert_manager,
  ]
}

# https://artifacthub.io/packages/helm/vultr/cert-manager-webhook-vultr
resource "helm_release" "cert_manager_webhook_vultr" {
  name       = "cert-manager-webhook-vultr"
  repository = "https://vultr.github.io/helm-charts"
  chart      = "cert-manager-webhook-vultr"
  version    = "1.0.0"

  namespace = kubernetes_namespace.cert_manager.metadata.0.name

  depends_on = [
    kubernetes_secret.vultr_domain,
    helm_release.cert_manager,
  ]
}

# TODO role bindings...
