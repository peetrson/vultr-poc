locals {
  vultr = {
    city = "Frankfurt"
  }
  k8s = {
    version = "v1.24.4+1"
    ingress = {
      class = "nginx"
    }
    cert_manager = {
      enabled = true
    }
  }
  out = {
    directory = "${path.root}/../out"
    k8s = {
      config = {
        host =  yamldecode(base64decode(vultr_kubernetes.k8s.kube_config))["clusters"][0]["cluster"]["server"],
        cluster_ca_certificate = base64decode(yamldecode(base64decode(vultr_kubernetes.k8s.kube_config))["clusters"][0]["cluster"]["certificate-authority-data"])
        client_certificate = base64decode(yamldecode(base64decode(vultr_kubernetes.k8s.kube_config))["users"][0]["user"]["client-certificate-data"])
        client_key = base64decode(yamldecode(base64decode(vultr_kubernetes.k8s.kube_config))["users"][0]["user"]["client-key-data"])
      }
      node_pool = {
        #ips = [for instance in data.vultr_instance.k8s_nodes : instance.main_ip]
        ips = []
      }
    }
  }
}
